SUMMARY
=======

Views Flipped Table Accordion generates views-based tables with arbitrary
groups of rows hidden. Display of the hidden rows can be toggled by
clicking the parent row.

For a full description of the module, visit the project page:
  http://drupal.org/project/views_flipped_table_accordion

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/views_flipped_table_accordion


REQUIREMENTS
------------

Install [Views Flipped Table](https://drupal.org/project/views_flipped_table) and all of it's dependencies.

INSTALLATION
------------

* Install as usual, see http://drupal.org/node/70151 for further information.

CONFIGURATION
-------------

* Create a view using the "Views Flipped Table Accordion" style, and add all
  the required fields.

* To create a row group, edit the parent field, and in the Style Settings 
  drop-down, select "Customize field and label wrapper HTML". Click "Create a
  CSS class", and add the class "Accordion". Rows below this row, until the next
  accordion row will be hidden by default, and can be displayed by clicking this
  row.

* To end an accordion group, either create a new one, in the same manner as
  above, or add the "not-accordion" CSS class to a row following an accordion
  group, in the same manner as described above.

CUSTOMIZATION
-------------

CSS can be customised in the same manner as a normal views table.

CONTACT
------

Current maintainers:

* naught101 - http://drupal.org/user/44216

