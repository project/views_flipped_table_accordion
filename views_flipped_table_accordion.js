(function ($) {
  Drupal.behaviors.views_flipped_table_accordion =  {
    attach: function(context, settings) {
      $(".view tr.accordion", context).nextUntil("tr.accordion, tr.not-accordion").hide();

      $(".view tr.accordion", context).click(function(){
        $(this).nextUntil("tr.accordion, tr.not-accordion").fadeToggle(500)
      });
    }
  }
}(jQuery));
