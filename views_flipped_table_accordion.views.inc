<?php
/**
 * @file views_flipped_table_accordion.views.inc
 * Contains the flipped table style plugin, flipping rows and columns.
 */

/**
 * Implementation of hook_views_plugins().
 */
function views_flipped_table_accordion_views_plugins() {
  return array(
    'style' => array(
      'flipped_accordion' => array(
        'title' => t('Flipped table accordion'),
        'help' => t('Displays a table with rows and columns flipped, with arbitary rows hidden with jquery.'),
        'handler' => 'views_flipped_table_plugin_style_flipped_table',
        'path' => drupal_get_path('module', 'views_flipped_table'),
        'parent' => 'views_flipped_table',
        'uses row plugin' => FALSE,
        'uses fields' => TRUE,
        'uses options' => TRUE,
        'type' => 'normal',
        'theme' => 'views_flipped_table',
        // 'theme path' => drupal_get_path('module', 'views_flipped_table'),
      ),
    ),
  );
}
